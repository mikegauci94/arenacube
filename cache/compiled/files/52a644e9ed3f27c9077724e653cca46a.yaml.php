<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/DaMike/Documents/repos/arenacube/user/plugins/admin/languages/lt.yaml',
    'modified' => 1467966304,
    'data' => [
        'PLUGIN_ADMIN' => [
            'MANAGE_PAGES' => 'Tvarkyti puslapius',
            'PAGES' => 'Puslapiai',
            'THEMES' => 'Temos',
            'BACK' => 'Atgal',
            'MOVE' => 'Perkelti',
            'DELETE' => 'Ištrinti',
            'SAVE' => 'Išsaugoti',
            'ERROR' => 'Klaida',
            'CANCEL' => 'Atšaukti',
            'CONTINUE' => 'Tęsti'
        ]
    ]
];
