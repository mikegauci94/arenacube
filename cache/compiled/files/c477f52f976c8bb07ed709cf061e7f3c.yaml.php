<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/DaMike/Documents/repos/arenacube/system/config/site.yaml',
    'modified' => 1467966303,
    'data' => [
        'taxonomies' => [
            0 => 'category',
            1 => 'tag'
        ],
        'metadata' => [
            'description' => 'Online iGaming Software Company offering products such as the multiplayer betting platform Arena Challenge, a fully managed sportsbook and a high performance Gaming Platform.'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 300,
            'delimiter' => '==='
        ],
        'redirects' => NULL,
        'routes' => NULL,
        'blog' => [
            'route' => '/blog'
        ]
    ]
];
