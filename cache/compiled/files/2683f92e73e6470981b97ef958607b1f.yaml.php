<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/DaMike/Documents/repos/arenacube/user/config/site.yaml',
    'modified' => 1467966303,
    'data' => [
        'footer' => [
            'title' => [
                'subscribe' => 'Subscribe to our newsletter',
                'follow' => 'Follow us'
            ],
            'support' => [
                0 => [
                    'text' => 'Contact Us',
                    'link' => 'contact'
                ],
                1 => [
                    'text' => 'Terms and Conditions',
                    'link' => 'terms-conditions'
                ],
                2 => [
                    'text' => 'Arenacube&copy;2015'
                ]
            ],
            'social' => [
                0 => [
                    'icon' => 'fa fa-facebook',
                    'link' => 'https://www.facebook.com/arenacubeltd/'
                ],
                1 => [
                    'icon' => 'fa fa-twitter',
                    'link' => 'https://twitter.com/arenacube'
                ],
                2 => [
                    'icon' => 'fa fa-google-plus',
                    'link' => 'https://plus.google.com/112668344430881222689/videos'
                ],
                3 => [
                    'icon' => 'fa fa-youtube',
                    'link' => 'https://www.youtube.com/user/arenacubeltd'
                ],
                4 => [
                    'icon' => 'fa fa-linkedin',
                    'link' => 'https://www.linkedin.com/company/arenacube'
                ]
            ]
        ]
    ]
];
