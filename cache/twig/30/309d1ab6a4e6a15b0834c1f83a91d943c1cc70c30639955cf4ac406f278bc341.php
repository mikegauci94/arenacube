<?php

/* error.html.twig */
class __TwigTemplate_c726c70013e765dba0dedbb14736aabcde9b59c19acba29d4a4f9a7c21000907 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "error.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
\t<div class=\"error-box\">
\t\t<div class=\"text-center\">
\t\t";
        // line 7
        if (($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "http_response_code", array()) == 404)) {
            // line 8
            echo "\t\t\t<h1>";
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
            echo "</h1>

\t\t\t<div class=\"follow-me\">
\t\t\t\t<a class=\"btn btn-success\" href=\"home\">";
            // line 11
            echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "button_text", array());
            echo "</a>
\t\t\t</div>

\t\t";
        } else {
            // line 15
            echo "\t\t\t<h1>Error ";
            echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "http_response_code", array());
            echo "</h1>
\t\t\t<p>
\t\t\t\t";
            // line 17
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
            echo "
\t\t\t</p>
\t\t";
        }
        // line 20
        echo "
\t\t</div>
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 20,  58 => 17,  52 => 15,  45 => 11,  38 => 8,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends 'partials/base.html.twig' %}*/
/* */
/* {% block content %}*/
/* <div class="container">*/
/* 	<div class="error-box">*/
/* 		<div class="text-center">*/
/* 		{% if (page.header.http_response_code == 404) %}*/
/* 			<h1>{{ page.content }}</h1>*/
/* */
/* 			<div class="follow-me">*/
/* 				<a class="btn btn-success" href="home">{{ page.header.button_text }}</a>*/
/* 			</div>*/
/* */
/* 		{% else %}*/
/* 			<h1>Error {{ page.header.http_response_code }}</h1>*/
/* 			<p>*/
/* 				{{ page.content }}*/
/* 			</p>*/
/* 		{% endif %}*/
/* */
/* 		</div>*/
/* 	</div>*/
/* </div>*/
/* {% endblock %}*/
/* */
