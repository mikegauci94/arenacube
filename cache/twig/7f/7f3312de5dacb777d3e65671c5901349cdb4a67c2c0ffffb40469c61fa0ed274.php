<?php

/* products.html.twig */
class __TwigTemplate_fe744bfb243ca9c7ccf5fc25c673ffb6d312d2aa19941ed5d8013e325a6d6fea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "products.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        // line 5
        echo "    <div class=\"jumbotron-products\" data-stellar-background-ratio=\"0.4\" >
        <div class=\"container\">
            <div class=\"image-caption\">
                <h1>";
        // line 8
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "products", array()), "heading", array());
        echo "</h1>
                <p>";
        // line 9
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "products", array()), "subheading", array());
        echo "</p>
            </div>
            <div class=\"products-masonary\">
                <div class=\"container products\">
                    <div class=\"row\">
                        <div class=\"col-md-offset-1 col-md-5\">
                            <div class=\"products-image arenachallenge\">
                                <div class=\"products-content\">
                                    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "first_products", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["first_product"]) {
            // line 18
            echo "                                    <img src=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["first_product"], "image", array()), array(), "array"), "url", array());
            echo "\">
                                    <h3>";
            // line 19
            echo $this->getAttribute($context["first_product"], "title", array());
            echo "</h3>
                                    <p>";
            // line 20
            echo $this->getAttribute($context["first_product"], "text", array());
            echo "</p>
                                    <a class=\"btn btn-primary btn-lg black\" href=\"";
            // line 21
            echo $this->getAttribute($context["first_product"], "link", array());
            echo "\" role=\"button\">";
            echo $this->getAttribute($context["first_product"], "button", array());
            echo "<i class=\"fa fa-angle-right\"></i></a>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['first_product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "                                </div>
                            </div>
                        </div>
                        <div class=\"col-md-5\">
                            <div class=\"products-image sportsbook\">
                                <div class=\"products-content\">
                                    ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "second_products", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["second_product"]) {
            // line 30
            echo "                                        <img src=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["second_product"], "image", array()), array(), "array"), "url", array());
            echo "\">
                                        <h3>";
            // line 31
            echo $this->getAttribute($context["second_product"], "title", array());
            echo "</h3>
                                        <p>";
            // line 32
            echo $this->getAttribute($context["second_product"], "text", array());
            echo "</p>
                                        <a class=\"btn btn-primary btn-lg black\" href=\"";
            // line 33
            echo $this->getAttribute($context["second_product"], "link", array());
            echo "\" role=\"button\">";
            echo $this->getAttribute($context["second_product"], "button", array());
            echo "<i class=\"fa fa-angle-right\"></i></a>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['second_product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-offset-1 col-md-5\">
                            <div class=\"products-image thirdeye\">
                                <div class=\"products-content\">
                                    ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "third_products", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["third_product"]) {
            // line 44
            echo "                                        <img src=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["third_product"], "image", array()), array(), "array"), "url", array());
            echo "\">
                                        <h3>";
            // line 45
            echo $this->getAttribute($context["third_product"], "title", array());
            echo "</h3>
                                        <p>";
            // line 46
            echo $this->getAttribute($context["third_product"], "text", array());
            echo "</p>
                                        <a class=\"btn btn-primary btn-lg black\" href=\"";
            // line 47
            echo $this->getAttribute($context["third_product"], "link", array());
            echo "\" role=\"button\">";
            echo $this->getAttribute($context["third_product"], "button", array());
            echo "<i class=\"fa fa-angle-right\"></i></a>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['third_product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                                </div>
                            </div>
                        </div>
                        <div class=\"col-md-5\">
                            <div class=\"products-image avacs\">
                                <div class=\"products-content\">
                                    ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "fourth_products", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["fourth_product"]) {
            // line 56
            echo "                                        <img src=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["fourth_product"], "image", array()), array(), "array"), "url", array());
            echo "\">
                                        <h3>";
            // line 57
            echo $this->getAttribute($context["fourth_product"], "title", array());
            echo "</h3>
                                        <p>";
            // line 58
            echo $this->getAttribute($context["fourth_product"], "text", array());
            echo "</p>
                                        <a class=\"btn btn-primary btn-lg black\" href=\"";
            // line 59
            echo $this->getAttribute($context["fourth_product"], "link", array());
            echo "\" role=\"button\">";
            echo $this->getAttribute($context["fourth_product"], "button", array());
            echo "<i class=\"fa fa-angle-right\"></i></a>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fourth_product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-offset-1 col-md-5\">
                            <div class=\"products-image widerpoker\">
                                <div class=\"products-content\">
                                    ";
        // line 69
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "fifth_products", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["fifth_product"]) {
            // line 70
            echo "                                        <img src=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["fifth_product"], "image", array()), array(), "array"), "url", array());
            echo "\">
                                        <h3>";
            // line 71
            echo $this->getAttribute($context["fifth_product"], "title", array());
            echo "</h3>
                                        <p>";
            // line 72
            echo $this->getAttribute($context["fifth_product"], "text", array());
            echo "</p>
                                        <a class=\"btn btn-primary btn-lg black\" href=\"";
            // line 73
            echo $this->getAttribute($context["fifth_product"], "link", array());
            echo "\" role=\"button\">";
            echo $this->getAttribute($context["fifth_product"], "button", array());
            echo "<i class=\"fa fa-angle-right\"></i></a>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fifth_product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "                                </div>
                            </div>
                        </div>
                        <div class=\"col-md-5\">
                            <div class=\"products-image gaming-platform\">
                                <div class=\"products-content\">
                                    ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "sixth_products", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["sixth_product"]) {
            // line 82
            echo "                                        <img src=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["sixth_product"], "image", array()), array(), "array"), "url", array());
            echo "\">
                                        <h3>";
            // line 83
            echo $this->getAttribute($context["sixth_product"], "title", array());
            echo "</h3>
                                        <p>";
            // line 84
            echo $this->getAttribute($context["sixth_product"], "text", array());
            echo "</p>
                                        <a class=\"btn btn-primary btn-lg black\" href=\"";
            // line 85
            echo $this->getAttribute($context["sixth_product"], "link", array());
            echo "\" role=\"button\">";
            echo $this->getAttribute($context["sixth_product"], "button", array());
            echo "<i class=\"fa fa-angle-right\"></i></a>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sixth_product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "products.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 87,  249 => 85,  245 => 84,  241 => 83,  236 => 82,  232 => 81,  224 => 75,  214 => 73,  210 => 72,  206 => 71,  201 => 70,  197 => 69,  187 => 61,  177 => 59,  173 => 58,  169 => 57,  164 => 56,  160 => 55,  152 => 49,  142 => 47,  138 => 46,  134 => 45,  129 => 44,  125 => 43,  115 => 35,  105 => 33,  101 => 32,  97 => 31,  92 => 30,  88 => 29,  80 => 23,  70 => 21,  66 => 20,  62 => 19,  57 => 18,  53 => 17,  42 => 9,  38 => 8,  33 => 5,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends 'partials/base.html.twig' %}*/
/* */
/* {% block banner %}*/
/*     {# Top Banner #}*/
/*     <div class="jumbotron-products" data-stellar-background-ratio="0.4" >*/
/*         <div class="container">*/
/*             <div class="image-caption">*/
/*                 <h1>{{ page.header.products.heading }}</h1>*/
/*                 <p>{{ page.header.products.subheading }}</p>*/
/*             </div>*/
/*             <div class="products-masonary">*/
/*                 <div class="container products">*/
/*                     <div class="row">*/
/*                         <div class="col-md-offset-1 col-md-5">*/
/*                             <div class="products-image arenachallenge">*/
/*                                 <div class="products-content">*/
/*                                     {% for first_product in page.header.first_products %}*/
/*                                     <img src="{{ page.media[first_product.image].url }}">*/
/*                                     <h3>{{ first_product.title }}</h3>*/
/*                                     <p>{{ first_product.text }}</p>*/
/*                                     <a class="btn btn-primary btn-lg black" href="{{ first_product.link }}" role="button">{{ first_product.button }}<i class="fa fa-angle-right"></i></a>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-5">*/
/*                             <div class="products-image sportsbook">*/
/*                                 <div class="products-content">*/
/*                                     {% for second_product in page.header.second_products %}*/
/*                                         <img src="{{ page.media[second_product.image].url }}">*/
/*                                         <h3>{{ second_product.title }}</h3>*/
/*                                         <p>{{ second_product.text }}</p>*/
/*                                         <a class="btn btn-primary btn-lg black" href="{{ second_product.link }}" role="button">{{ second_product.button }}<i class="fa fa-angle-right"></i></a>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-md-offset-1 col-md-5">*/
/*                             <div class="products-image thirdeye">*/
/*                                 <div class="products-content">*/
/*                                     {% for third_product in page.header.third_products %}*/
/*                                         <img src="{{ page.media[third_product.image].url }}">*/
/*                                         <h3>{{ third_product.title }}</h3>*/
/*                                         <p>{{ third_product.text }}</p>*/
/*                                         <a class="btn btn-primary btn-lg black" href="{{ third_product.link }}" role="button">{{ third_product.button }}<i class="fa fa-angle-right"></i></a>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-5">*/
/*                             <div class="products-image avacs">*/
/*                                 <div class="products-content">*/
/*                                     {% for fourth_product in page.header.fourth_products %}*/
/*                                         <img src="{{ page.media[fourth_product.image].url }}">*/
/*                                         <h3>{{ fourth_product.title }}</h3>*/
/*                                         <p>{{ fourth_product.text }}</p>*/
/*                                         <a class="btn btn-primary btn-lg black" href="{{ fourth_product.link }}" role="button">{{ fourth_product.button }}<i class="fa fa-angle-right"></i></a>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="row">*/
/*                         <div class="col-md-offset-1 col-md-5">*/
/*                             <div class="products-image widerpoker">*/
/*                                 <div class="products-content">*/
/*                                     {% for fifth_product in page.header.fifth_products %}*/
/*                                         <img src="{{ page.media[fifth_product.image].url }}">*/
/*                                         <h3>{{ fifth_product.title }}</h3>*/
/*                                         <p>{{ fifth_product.text }}</p>*/
/*                                         <a class="btn btn-primary btn-lg black" href="{{ fifth_product.link }}" role="button">{{ fifth_product.button }}<i class="fa fa-angle-right"></i></a>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-md-5">*/
/*                             <div class="products-image gaming-platform">*/
/*                                 <div class="products-content">*/
/*                                     {% for sixth_product in page.header.sixth_products %}*/
/*                                         <img src="{{ page.media[sixth_product.image].url }}">*/
/*                                         <h3>{{ sixth_product.title }}</h3>*/
/*                                         <p>{{ sixth_product.text }}</p>*/
/*                                         <a class="btn btn-primary btn-lg black" href="{{ sixth_product.link }}" role="button">{{ sixth_product.button }}<i class="fa fa-angle-right"></i></a>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/* {% endblock %}*/
/* */
/* */
/* */
/* */
