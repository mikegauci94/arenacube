<?php

/* contact.html.twig */
class __TwigTemplate_d6d06742081cf3f635ccef1cc437552aeded506106d887be9cc3a16d5bf43544 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "contact.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        // line 5
        echo "    <div class=\"jumbotron-banner contact\">
        <div class=\"pixel-overlay banner\"></div>
    </div>
";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        // line 11
        echo "<div class=\"contact-content\" data-stellar-background-ratio=\"0.4\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <h1>";
        // line 15
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "contact", array()), "heading", array());
        echo "</h1>
            </div>

            ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "contact_area", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
            // line 19
            echo "            <div class=\"col-md-4\">
                <div class=\"image-wrap\">
                    <img src=\"";
            // line 21
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["contact"], "image", array()), array(), "array"), "url", array());
            echo "\">
                    <div class=\"image-content\">
                        <h3 class=\"image-title\">";
            // line 23
            echo $this->getAttribute($context["contact"], "title", array());
            echo "</h3>
                        <div class=\"image-caption\">
                            <p>";
            // line 25
            echo $this->getAttribute($context["contact"], "address", array());
            echo "</p>
                        </div>
                    </div>
                    <iframe src=\"";
            // line 28
            echo $this->getAttribute($context["contact"], "map", array());
            echo "\" width=\"100%\" height=\"200px\" draggable=\"false\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
                </div>
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "
            <div class=\"col-md-12\">
                <p>";
        // line 34
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "contact_details", array()), "line_1", array());
        echo "</p>
                <p>";
        // line 35
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "contact_details", array()), "line_2", array());
        echo "</p>
                    <div id=\"contact-form\">
                        ";
        // line 37
        $this->loadTemplate("forms/form.html.twig", "contact.html.twig", 37)->display($context);
        // line 38
        echo "                    </div>
                <p>";
        // line 39
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "contact_details", array()), "line_3", array());
        echo "</p>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 39,  107 => 38,  105 => 37,  100 => 35,  96 => 34,  92 => 32,  82 => 28,  76 => 25,  71 => 23,  66 => 21,  62 => 19,  58 => 18,  52 => 15,  46 => 11,  44 => 10,  41 => 9,  34 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends 'partials/base.html.twig' %}*/
/* */
/* {% block banner %}*/
/*     {# Top Banner #}*/
/*     <div class="jumbotron-banner contact">*/
/*         <div class="pixel-overlay banner"></div>*/
/*     </div>*/
/* {% endblock %}*/
/* {% block content %}*/
/*     {# Main Content #}*/
/* <div class="contact-content" data-stellar-background-ratio="0.4">*/
/*     <div class="container">*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 <h1>{{ page.header.contact.heading }}</h1>*/
/*             </div>*/
/* */
/*             {% for contact in page.header.contact_area %}*/
/*             <div class="col-md-4">*/
/*                 <div class="image-wrap">*/
/*                     <img src="{{ page.media[contact.image].url }}">*/
/*                     <div class="image-content">*/
/*                         <h3 class="image-title">{{ contact.title }}</h3>*/
/*                         <div class="image-caption">*/
/*                             <p>{{ contact.address }}</p>*/
/*                         </div>*/
/*                     </div>*/
/*                     <iframe src="{{ contact.map }}" width="100%" height="200px" draggable="false" frameborder="0" style="border:0" allowfullscreen></iframe>*/
/*                 </div>*/
/*             </div>*/
/*             {% endfor %}*/
/* */
/*             <div class="col-md-12">*/
/*                 <p>{{ page.header.contact_details.line_1 }}</p>*/
/*                 <p>{{ page.header.contact_details.line_2 }}</p>*/
/*                     <div id="contact-form">*/
/*                         {% include "forms/form.html.twig" %}*/
/*                     </div>*/
/*                 <p>{{ page.header.contact_details.line_3 }}</p>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* */
