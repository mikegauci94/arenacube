<?php

/* partials/base.html.twig */
class __TwigTemplate_6c230915d178cb9a9ba89fe1b8c12b9af69bf5a38aa26ba81f096d772500e58d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'banner' => array($this, 'block_banner'),
            'content' => array($this, 'block_content'),
            'bottom' => array($this, 'block_bottom'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 52
        echo "</head>

<body>

";
        // line 56
        $this->loadTemplate("partials/navigation.html.twig", "partials/base.html.twig", 56)->display($context);
        // line 57
        echo "

<div class=\"top-banner\">
    ";
        // line 60
        $this->displayBlock('banner', $context, $blocks);
        // line 61
        echo "</div>

";
        // line 64
        echo "    ";
        $this->displayBlock('content', $context, $blocks);
        // line 66
        echo "
";
        // line 68
        echo "

";
        // line 71
        echo "<footer>
";
        // line 72
        $this->loadTemplate("partials/footer.html.twig", "partials/base.html.twig", 72)->display($context);
        // line 73
        echo "</footer>

</body>
";
        // line 76
        $this->displayBlock('bottom', $context, $blocks);
        // line 77
        echo "</html>";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\">
        ";
        // line 8
        if ($this->getAttribute((isset($context["header"]) ? $context["header"] : null), "description", array())) {
            // line 9
            echo "            <meta name=\"description\" content=\"";
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "description", array());
            echo "\">
        ";
        } else {
            // line 11
            echo "            <meta name=\"description\" content=\"";
            echo $this->getAttribute((isset($context["site"]) ? $context["site"] : null), "description", array());
            echo "\">
        ";
        }
        // line 13
        echo "        ";
        if ($this->getAttribute((isset($context["header"]) ? $context["header"] : null), "robots", array())) {
            // line 14
            echo "            <meta name=\"robots\" content=\"";
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "robots", array());
            echo "\">
        ";
        }
        // line 16
        echo "        <link rel=\"icon\" type=\"image/ico\" href=\"";
        echo (isset($context["theme_url"]) ? $context["theme_url"] : null);
        echo "/images/favicon.ico\">

        <title>";
        // line 18
        if ($this->getAttribute((isset($context["header"]) ? $context["header"] : null), "title", array())) {
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "title", array());
        }
        echo $this->getAttribute((isset($context["site"]) ? $context["site"] : null), "title", array());
        echo "</title>
        <link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        ";
        // line 21
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 33
        echo "
        ";
        // line 34
        $this->displayBlock('javascripts', $context, $blocks);
        // line 50
        echo "
    ";
    }

    // line 21
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 22
        echo "            ";
        // line 23
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://css/bootstrap.min.css", 1 => 101), "method");
        // line 24
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://css/carousel.css", 1 => 101), "method");
        // line 25
        echo "
            ";
        // line 27
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://css/bootstrap-custom.css", 1 => 100), "method");
        // line 28
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://css/font-awesome.min.css", 1 => 100), "method");
        // line 29
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://scss/skin-custom.css", 1 => 100), "method");
        // line 30
        echo "
            ";
        // line 31
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "css", array(), "method");
        echo "
        ";
    }

    // line 34
    public function block_javascripts($context, array $blocks = array())
    {
        // line 35
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js", 1 => 101), "method");
        // line 36
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://js/bootstrap.js"), "method");
        // line 37
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://js/jquery.nicescroll.js"), "method");
        // line 38
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://js/jquery.stellar.js"), "method");
        // line 39
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://js/script.js"), "method");
        // line 40
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://js/iscroll.js"), "method");
        // line 41
        echo "            ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "theme://js/jquery.min.js"), "method");
        // line 42
        echo "
            ";
        // line 43
        if (((($this->getAttribute((isset($context["browser"]) ? $context["browser"] : null), "getBrowser", array()) == "msie") && ($this->getAttribute((isset($context["browser"]) ? $context["browser"] : null), "getVersion", array()) >= 8)) && ($this->getAttribute((isset($context["browser"]) ? $context["browser"] : null), "getVersion", array()) <= 9))) {
            // line 44
            echo "                ";
            $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"), "method");
            // line 45
            echo "                ";
            $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "add", array(0 => "https://oss.maxcdn.com/respond/1.4.2/respond.min.js"), "method");
            // line 46
            echo "            ";
        }
        // line 47
        echo "
            ";
        // line 48
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "js", array(), "method");
        echo "
        ";
    }

    // line 60
    public function block_banner($context, array $blocks = array())
    {
    }

    // line 64
    public function block_content($context, array $blocks = array())
    {
    }

    // line 76
    public function block_bottom($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 76,  219 => 64,  214 => 60,  208 => 48,  205 => 47,  202 => 46,  199 => 45,  196 => 44,  194 => 43,  191 => 42,  188 => 41,  185 => 40,  182 => 39,  179 => 38,  176 => 37,  173 => 36,  170 => 35,  167 => 34,  161 => 31,  158 => 30,  155 => 29,  152 => 28,  149 => 27,  146 => 25,  143 => 24,  140 => 23,  138 => 22,  135 => 21,  130 => 50,  128 => 34,  125 => 33,  123 => 21,  114 => 18,  108 => 16,  102 => 14,  99 => 13,  93 => 11,  87 => 9,  85 => 8,  80 => 5,  77 => 4,  73 => 77,  71 => 76,  66 => 73,  64 => 72,  61 => 71,  57 => 68,  54 => 66,  51 => 64,  47 => 61,  45 => 60,  40 => 57,  38 => 56,  32 => 52,  30 => 4,  25 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     {% block head %}*/
/*         <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*         <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">*/
/*         {% if header.description %}*/
/*             <meta name="description" content="{{ header.description }}">*/
/*         {% else %}*/
/*             <meta name="description" content="{{ site.description }}">*/
/*         {% endif %}*/
/*         {% if header.robots %}*/
/*             <meta name="robots" content="{{ header.robots }}">*/
/*         {% endif %}*/
/*         <link rel="icon" type="image/ico" href="{{ theme_url }}/images/favicon.ico">*/
/* */
/*         <title>{% if header.title %}{{ header.title }}{% endif %}{{ site.title }}</title>*/
/*         <link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>*/
/*         <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>*/
/*         {% block stylesheets %}*/
/*             {# Bootstrap core CSS #}*/
/*             {% do assets.add('theme://css/bootstrap.min.css',101) %}*/
/*             {% do assets.add('theme://css/carousel.css',101) %}*/
/* */
/*             {# Custom styles for this theme #}*/
/*             {% do assets.add('theme://css/bootstrap-custom.css',100) %}*/
/*             {% do assets.add('theme://css/font-awesome.min.css',100) %}*/
/*             {% do assets.add('theme://scss/skin-custom.css',100) %}*/
/* */
/*             {{ assets.css() }}*/
/*         {% endblock %}*/
/* */
/*         {% block javascripts %}*/
/*             {% do assets.add('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', 101) %}*/
/*             {% do assets.add('theme://js/bootstrap.js') %}*/
/*             {% do assets.add('theme://js/jquery.nicescroll.js') %}*/
/*             {% do assets.add('theme://js/jquery.stellar.js') %}*/
/*             {% do assets.add('theme://js/script.js') %}*/
/*             {% do assets.add('theme://js/iscroll.js') %}*/
/*             {% do assets.add('theme://js/jquery.min.js') %}*/
/* */
/*             {% if browser.getBrowser == 'msie' and browser.getVersion >= 8 and browser.getVersion <= 9 %}*/
/*                 {% do assets.add('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') %}*/
/*                 {% do assets.add('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') %}*/
/*             {% endif %}*/
/* */
/*             {{ assets.js() }}*/
/*         {% endblock %}*/
/* */
/*     {% endblock head %}*/
/* </head>*/
/* */
/* <body>*/
/* */
/* {% include 'partials/navigation.html.twig' %}*/
/* */
/* */
/* <div class="top-banner">*/
/*     {% block banner %}{% endblock %}*/
/* </div>*/
/* */
/* {#<div class="container marketing">#}*/
/*     {% block content %}{% endblock %}*/
/* {#</div>#}*/
/* */
/* {# include the _footer content #}*/
/* */
/* */
/* {# include the _footer content #}*/
/* <footer>*/
/* {% include 'partials/footer.html.twig' %}*/
/* </footer>*/
/* */
/* </body>*/
/* {% block bottom %}{% endblock %}*/
/* </html>*/
