<?php

/* home.html.twig */
class __TwigTemplate_daef9f3f9af2932cd485332d26eefb4973201006e279b9c5c5526c4ece113cfb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "home.html.twig", 1);
        $this->blocks = array(
            'banner' => array($this, 'block_banner'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_banner($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        // line 5
        echo "    <div class=\"jumbotron\" id=\"wrapper\" data-stellar-background-ratio=\"0.4\">
        <div id=\"scroller\">
        <div class=\"pixel-overlay\"></div>
        <div class=\"container\">
            <div class=\"image-caption\">
                <h1>";
        // line 10
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_banner", array()), "header_top", array());
        echo "</h1>
            </div>
        </div>
        <div class=\"bottom-banner-container\">
            <div class=\"container\">
                <a href=\"";
        // line 15
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_banner", array()), "link", array());
        echo "\" target=\"_blank\"><img src=\"";
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "shortlisted.png", array(), "array"), "url", array());
        echo "\"></a>
                <p>";
        // line 16
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_banner", array()), "header_bottom", array());
        echo "</p>
                <a class=\"btn btn-primary btn-lg black\" href=\"";
        // line 17
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_banner", array()), "link", array());
        echo "\" role=\"button\">";
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_banner", array()), "button", array());
        echo "<i class=\"fa fa-angle-right\"></i></a>
            </div>
        </div>
            </div>
    </div>
";
    }

    // line 24
    public function block_content($context, array $blocks = array())
    {
        // line 25
        echo "    ";
        // line 26
        echo "    <div class=\"middle-content-background\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-8\">
                    <img class=\"home-feature-image\" src=\"";
        // line 30
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "arena-challenge-1.png", array(), "array"), "url", array());
        echo "\">
                </div>
                <div class=\"col-sm-4\">
                    <div class=\"home-arena-challenge-caption\">
                        <h2>";
        // line 34
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_arena_challenge_caption", array()), "title", array());
        echo "</h2>
                        <h3>";
        // line 35
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_arena_challenge_caption", array()), "sub_title", array());
        echo "</h3>
                        <p>";
        // line 36
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_arena_challenge_caption", array()), "description", array());
        echo "</p>
                        <a class=\"btn btn-primary btn-lg black\" href=\"";
        // line 37
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_arena_challenge_caption", array()), "link", array());
        echo "\" role=\"button\">";
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_banner", array()), "button", array());
        echo "<i class=\"fa fa-angle-right\"></i></a>
                    </div>
                </div>
            </div>
                <div class=\"row\">
                    <div class=\"col-sm-offset-4 col-sm-8\">
                        <div class=\"outerholder\">
                            ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "arena_challenge_features", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["arena_challenge_feature"]) {
            // line 45
            echo "                            <div class=\"floating-box\">
                                <div class=\"image\">
                                    <img class=\"image\" src=\"";
            // line 47
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["arena_challenge_feature"], "image", array()), array(), "array"), "url", array());
            echo "\">
                                </div>
                                <div class=\"floating-text\">
                                    <div class=\"value\">
                                        ";
            // line 51
            echo $this->getAttribute($context["arena_challenge_feature"], "title", array());
            echo "
                                    </div>
                                    <div class=\"description\">
                                        ";
            // line 54
            echo $this->getAttribute($context["arena_challenge_feature"], "description", array());
            echo "
                                    </div>
                                </div>
                            </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arena_challenge_feature'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class=\"third-content-background\" data-stellar-background-ratio=\"0.4\">
        <div class=\"pixel-overlay\"></div>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-4\">
                    <div class=\"home-sportsbook-caption\">
                        <h2>";
        // line 71
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_sportsbook_caption", array()), "title", array());
        echo "</h2>
                        <p>";
        // line 72
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_sportsbook_caption", array()), "description", array());
        echo "</p>
                        <a class=\"btn btn-primary btn-lg black\" href=\"";
        // line 73
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_sportsbook_caption", array()), "link", array());
        echo "\" role=\"button\">";
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "home_sportsbook_caption", array()), "button", array());
        echo "<i class=\"fa fa-angle-right\"></i></a>
                    </div>
                </div>
                <div class=\"col-sm-8\">
                    <img class=\"home-feature-image\" src=\"";
        // line 77
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "sb-homepage.png", array(), "array"), "url", array());
        echo "\">
                </div>
            </div>
        </div>
    </div>

<div class=\"last-content-background\">
    <div class=\"container\">
        <div class=\"row\">
                <h2>";
        // line 86
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "partners", array()), "title", array());
        echo "</h2>
            <div class=\"partner-logos\">
                <div class=\"logo wintech\"></div>
                <div class=\"logo betradar\"></div>
                <div class=\"logo lambda\"></div>
                <div class=\"logo apcopay\"></div>
            </div>
                <h2>";
        // line 93
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "featured", array()), "title", array());
        echo "</h2>
            <div class=\"feature-logos\">
                <a href=\"";
        // line 95
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "featured", array()), "link_one", array());
        echo "\" target=\"_blank\"><div class=\"logo feature gambling\"></div></a>
                <a href=\"";
        // line 96
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "featured", array()), "link_two", array());
        echo "\" target=\"_blank\"><div class=\"logo feature igaming\"></div></a>
                <a href=\"";
        // line 97
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "featured", array()), "link_three", array());
        echo "\" target=\"_blank\"><div class=\"logo feature fortune\"></div></a>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 97,  207 => 96,  203 => 95,  198 => 93,  188 => 86,  176 => 77,  167 => 73,  163 => 72,  159 => 71,  145 => 59,  134 => 54,  128 => 51,  121 => 47,  117 => 45,  113 => 44,  101 => 37,  97 => 36,  93 => 35,  89 => 34,  82 => 30,  76 => 26,  74 => 25,  71 => 24,  59 => 17,  55 => 16,  49 => 15,  41 => 10,  34 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends 'partials/base.html.twig' %}*/
/* */
/* {% block banner %}*/
/*     {# Top Banner #}*/
/*     <div class="jumbotron" id="wrapper" data-stellar-background-ratio="0.4">*/
/*         <div id="scroller">*/
/*         <div class="pixel-overlay"></div>*/
/*         <div class="container">*/
/*             <div class="image-caption">*/
/*                 <h1>{{ page.header.home_banner.header_top }}</h1>*/
/*             </div>*/
/*         </div>*/
/*         <div class="bottom-banner-container">*/
/*             <div class="container">*/
/*                 <a href="{{ page.header.home_banner.link }}" target="_blank"><img src="{{ page.media['shortlisted.png'].url }}"></a>*/
/*                 <p>{{ page.header.home_banner.header_bottom }}</p>*/
/*                 <a class="btn btn-primary btn-lg black" href="{{ page.header.home_banner.link }}" role="button">{{ page.header.home_banner.button }}<i class="fa fa-angle-right"></i></a>*/
/*             </div>*/
/*         </div>*/
/*             </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     {# Main Content #}*/
/*     <div class="middle-content-background">*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-sm-8">*/
/*                     <img class="home-feature-image" src="{{ page.media['arena-challenge-1.png'].url }}">*/
/*                 </div>*/
/*                 <div class="col-sm-4">*/
/*                     <div class="home-arena-challenge-caption">*/
/*                         <h2>{{ page.header.home_arena_challenge_caption.title }}</h2>*/
/*                         <h3>{{ page.header.home_arena_challenge_caption.sub_title }}</h3>*/
/*                         <p>{{ page.header.home_arena_challenge_caption.description }}</p>*/
/*                         <a class="btn btn-primary btn-lg black" href="{{ page.header.home_arena_challenge_caption.link }}" role="button">{{ page.header.home_banner.button }}<i class="fa fa-angle-right"></i></a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*                 <div class="row">*/
/*                     <div class="col-sm-offset-4 col-sm-8">*/
/*                         <div class="outerholder">*/
/*                             {% for arena_challenge_feature in page.header.arena_challenge_features %}*/
/*                             <div class="floating-box">*/
/*                                 <div class="image">*/
/*                                     <img class="image" src="{{ page.media[arena_challenge_feature.image].url }}">*/
/*                                 </div>*/
/*                                 <div class="floating-text">*/
/*                                     <div class="value">*/
/*                                         {{ arena_challenge_feature.title }}*/
/*                                     </div>*/
/*                                     <div class="description">*/
/*                                         {{ arena_challenge_feature.description }}*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             {% endfor %}*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*     <div class="third-content-background" data-stellar-background-ratio="0.4">*/
/*         <div class="pixel-overlay"></div>*/
/*         <div class="container">*/
/*             <div class="row">*/
/*                 <div class="col-sm-4">*/
/*                     <div class="home-sportsbook-caption">*/
/*                         <h2>{{ page.header.home_sportsbook_caption.title }}</h2>*/
/*                         <p>{{ page.header.home_sportsbook_caption.description }}</p>*/
/*                         <a class="btn btn-primary btn-lg black" href="{{ page.header.home_sportsbook_caption.link }}" role="button">{{ page.header.home_sportsbook_caption.button }}<i class="fa fa-angle-right"></i></a>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="col-sm-8">*/
/*                     <img class="home-feature-image" src="{{ page.media['sb-homepage.png'].url }}">*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* <div class="last-content-background">*/
/*     <div class="container">*/
/*         <div class="row">*/
/*                 <h2>{{ page.header.partners.title }}</h2>*/
/*             <div class="partner-logos">*/
/*                 <div class="logo wintech"></div>*/
/*                 <div class="logo betradar"></div>*/
/*                 <div class="logo lambda"></div>*/
/*                 <div class="logo apcopay"></div>*/
/*             </div>*/
/*                 <h2>{{ page.header.featured.title }}</h2>*/
/*             <div class="feature-logos">*/
/*                 <a href="{{ page.header.featured.link_one }}" target="_blank"><div class="logo feature gambling"></div></a>*/
/*                 <a href="{{ page.header.featured.link_two }}" target="_blank"><div class="logo feature igaming"></div></a>*/
/*                 <a href="{{ page.header.featured.link_three }}" target="_blank"><div class="logo feature fortune"></div></a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* */
