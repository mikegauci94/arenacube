<?php

/* partials/footer.html.twig */
class __TwigTemplate_278f15260ec7780390d30a22977948382535ba03db1dbb67189873a3125912ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"footer-widgets\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4\">
                <h4>";
        // line 5
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "footer", array()), "title", array()), "subscribe", array());
        echo "</h4>
                ";
        // line 7
        echo "                    ";
        // line 8
        echo "                    ";
        // line 9
        echo "                ";
        // line 10
        echo "                <!-- Begin MailChimp Signup Form -->
                <link href=\"//cdn-images.mailchimp.com/embedcode/classic-10_7.css\" rel=\"stylesheet\" type=\"text/css\">
                <style type=\"text/css\">
                    #mc_embed_signup{clear:left; font-size:14px; }
                </style>
                <div id=\"mc_embed_signup\">
                    <form action=\"//arenacube.us13.list-manage.com/subscribe/post?u=aaf4d7226b437fd5644e572ef&amp;id=01aa9b7048\" method=\"post\" id=\"mc-embedded-subscribe-form\" name=\"mc-embedded-subscribe-form\" class=\"validate\" target=\"_blank\" novalidate>
                        <div id=\"mc_embed_signup_scroll\">
                            <h2>SUBSCRIBE TO OUR NEWSLETTER</h2>
                            <div class=\"mc-field-group\">
                                ";
        // line 21
        echo "                                ";
        // line 22
        echo "                                <input type=\"email\" value=\"\" name=\"EMAIL\" placeholder=\"Enter your email address here\" class=\"required email\" id=\"mce-EMAIL\">
                            </div>
                            <div id=\"mce-responses\" class=\"clear\">
                                <div class=\"response\" id=\"mce-error-response\" style=\"display:none\"></div>
                                <div class=\"response\" id=\"mce-success-response\" style=\"display:none\"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style=\"position: absolute; left: -5000px;\" aria-hidden=\"true\"><input type=\"text\" name=\"b_aaf4d7226b437fd5644e572ef_01aa9b7048\" tabindex=\"-1\" value=\"\"></div>
                            <div class=\"clear\"><input type=\"submit\" value=\"Subscribe\" name=\"subscribe\" id=\"mc-embedded-subscribe\" class=\"button\"></div>
                        </div>
                    </form>
                </div>
                <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function(\$) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var \$mcj = jQuery.noConflict(true);</script>
                <!--End mc_embed_signup-->
            </div>
            <div class=\"col-sm-4\">
                <a href=\"";
        // line 37
        echo ((((isset($context["base_url"]) ? $context["base_url"] : null) == "")) ? ("/") : ((isset($context["base_url"]) ? $context["base_url"] : null)));
        echo "\"><img class=\"footer-logo\" src=\"";
        echo (isset($context["theme_url"]) ? $context["theme_url"] : null);
        echo "/images/arenacube-logo.png\" alt=\"\"></a>
                <div class=\"footer-support\">
                    <ul>
                        ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "footer", array()), "support", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            // line 41
            echo "                            <li><a href=\"";
            echo $this->getAttribute($context["module"], "link", array());
            echo "\">";
            echo $this->getAttribute($context["module"], "text", array());
            echo "</a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "                    </ul>
                </div>
            </div>
        <div class=\"col-sm-4\">
            <h4>";
        // line 47
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "footer", array()), "title", array()), "follow", array());
        echo "</h4>
                <div class=\"social-container\">
                    <ul class=\"footer-social-icons\">
                        ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["site"]) ? $context["site"] : null), "footer", array()), "social", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            // line 51
            echo "                            <li><a href=\"";
            echo $this->getAttribute($context["module"], "link", array());
            echo "\" target=\"_blank\"><i class=\"";
            echo $this->getAttribute($context["module"], "icon", array());
            echo "\"></i></a></li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 53,  105 => 51,  101 => 50,  95 => 47,  89 => 43,  78 => 41,  74 => 40,  66 => 37,  49 => 22,  47 => 21,  35 => 10,  33 => 9,  31 => 8,  29 => 7,  25 => 5,  19 => 1,);
    }
}
/* <div class="footer-widgets">*/
/*     <div class="container">*/
/*         <div class="row">*/
/*             <div class="col-sm-4">*/
/*                 <h4>{{ site.footer.title.subscribe }}</h4>*/
/*                 {#<div class="form">#}*/
/*                     {#<input class="newsletter-email" type="email" required="" name="ne" value="Email" onclick="if (this.defaultValue==this.value) this.value=''" onblur="if (this.value=='') this.value=this.defaultValue">#}*/
/*                     {#<a href=""><img class="go-footer" src="{{ theme_url }}/images/go-footer.png" alt=""></a>#}*/
/*                 {#</div>#}*/
/*                 <!-- Begin MailChimp Signup Form -->*/
/*                 <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">*/
/*                 <style type="text/css">*/
/*                     #mc_embed_signup{clear:left; font-size:14px; }*/
/*                 </style>*/
/*                 <div id="mc_embed_signup">*/
/*                     <form action="//arenacube.us13.list-manage.com/subscribe/post?u=aaf4d7226b437fd5644e572ef&amp;id=01aa9b7048" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>*/
/*                         <div id="mc_embed_signup_scroll">*/
/*                             <h2>SUBSCRIBE TO OUR NEWSLETTER</h2>*/
/*                             <div class="mc-field-group">*/
/*                                 {#<label for="mce-EMAIL">Email Address#}*/
/*                                 {#</label>#}*/
/*                                 <input type="email" value="" name="EMAIL" placeholder="Enter your email address here" class="required email" id="mce-EMAIL">*/
/*                             </div>*/
/*                             <div id="mce-responses" class="clear">*/
/*                                 <div class="response" id="mce-error-response" style="display:none"></div>*/
/*                                 <div class="response" id="mce-success-response" style="display:none"></div>*/
/*                             </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->*/
/*                             <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_aaf4d7226b437fd5644e572ef_01aa9b7048" tabindex="-1" value=""></div>*/
/*                             <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>*/
/*                         </div>*/
/*                     </form>*/
/*                 </div>*/
/*                 <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>*/
/*                 <!--End mc_embed_signup-->*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*                 <a href="{{ base_url == '' ? '/' : base_url }}"><img class="footer-logo" src="{{ theme_url }}/images/arenacube-logo.png" alt=""></a>*/
/*                 <div class="footer-support">*/
/*                     <ul>*/
/*                         {% for module in site.footer.support %}*/
/*                             <li><a href="{{ module.link }}">{{ module.text }}</a></li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
/*             </div>*/
/*         <div class="col-sm-4">*/
/*             <h4>{{ site.footer.title.follow }}</h4>*/
/*                 <div class="social-container">*/
/*                     <ul class="footer-social-icons">*/
/*                         {% for module in site.footer.social %}*/
/*                             <li><a href="{{ module.link }}" target="_blank"><i class="{{ module.icon }}"></i></a></li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
