---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: Gambling Insider Magazine For ArenaCube: Sparking A Transformation
   subheading: Related posts
   interview_link: https://www.gamblinginsider.com/Magazine/SportsBettingFocus3/?Mid=55
   next:
        related_link: ArenaCube At ICE Totally Gaming
        link: news-arenacube-ice
   prev:
        related_link: “Our Mission Is To Disrupt And Innovate". As Featured In iGaming Business Magazine
        link: news-featured-igaming

news_paragraph:
    -
        text: Our co-founder Mr. Nikkos J. Frangos discusses our attempts to innovate betting without corrupting its roots: "..It was vital for us to keep the roots of betting uncorrupted, and innovate around these roots, whilst introducing competition in a way never done before. But even more than any of these was the yearning of novelty, and the passion to spark a transformation that the betting world needs…"
    -
        text: 'Read the full interview <a href="https://www.gamblinginsider.com/Magazine/SportsBettingFocus3/?Mid=55" target= "_blank"> here</a>'      
      
related_posts:
    -
        image: arenachallenge-news.jpg
        title: ArenaCube Announces Partnership With JVsolutions
        text: ArenaCube, an international iGaming software house, announces that a new partnership has been granted with iGaming solutions supplier JVSolutions, a prominent player in the robust Italian and international market ...
        link: news-arenacube-partnership
    -
        image: fortune-greece-news.png
        title: Fortune Greece Features Arenacube
        text: The Greek edition of the international magazine Fortune hosted our co-founder for an in-depth interview regarding ArenaCube and the next steps of our innovative game Arena Challenge ...
        link: news-fortune-greece-arenacube  
---

