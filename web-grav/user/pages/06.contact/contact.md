---
menu: Contact
title: Contact us - ArenaCube
description: 'Contact Us. 8 Alimou Av. Athens, 17 455 , Greece. Phone: +30 210 9816455, Emai: sales@arenacube.com'
contact:
    heading: 'Contact'
contact_area:
    -
        image: contact-greece.jpg
        title: Greece
        address: 8 Alimou Av, 17455, Athens, Greece,<em></em>+30 210 9816455
        map: https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3148.0323983569942!2d23.716782851003686!3d37.90630541225439!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14a1be7cd81aaad5%3A0xbbf6beee75a9840a!2sLeof.+Alimou+8%2C+Alimos+174+55%2C+Greece!5e0!3m2!1sen!2sus!4v1466430288880
    -
        image: contact-malta.jpg
        title: Malta
        address: Villa Ichang, Triq Mons Alfredo Mifsud, Ta’ Xbiex, Malta,+356 2713 3133
        map: https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1261.9828316812575!2d14.498374766804112!3d35.898611568822695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x130e4534e8f20695%3A0x1d570081ef5d4387!2sTriq+Mons+Alfredo+Mifsud%2C+Ta&#39;+Xbiex%2C+Malta!5e0!3m2!1sen!2sus!4v1466430523406
    -
        image: contact-italy.jpg
        title: Italy
        address: Via C De Giorgi, 29, 73100 Lecce, Italy,+39 0832 498158
        map: https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2919.6316503058374!2d18.17823220545804!3d40.36150488605417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13442ef1f61b10f9%3A0x13aee2a9a0b611d8!2sVia+Cosimo+de+Giorgi%2C+29%2C+73100+Lecce+LE%2C+Italy!5e0!3m2!1sen!2sus!4v1466430614750

contact_details:
    line_1: ARENACUBE P.C. – GENERAL ELECTRONIC COMMERCIAL REGISTRY: 127397902000
    line_2: We would like to hear from you… If you wish to contact us please email us at sales@arenacube.com or fill the following form:
    line_3: If you have any inquiry regarding Media or Marketing, please contact Marketing Department at marketing@arenacube.com           

form:
    content_after: 'or contact us directly at: | $email_support_link'
    name: contact-form
    classes: contact-form
    fields:
        -
            name: name
            label: Name
            placeholder: 'What is your name?'
            autocomplete: 'on'
            type: text
            validate:
                required: true
        -
            name: email
            label: Email
            placeholder: 'What is your email?'
            type: email
            validate:
                rule: email
                required: true
        -
            name: message
            label: Message
            placeholder: 'What do you like to share with us?'
            type: textarea
            validate:
                required: true
    buttons:
        -
            type: submit
            value: Send
            classes: 'black btn-primary'
    process:
        -
            email:
                from: '{{ config.email.from }}'
                to: ['{{ config.email.from }}', '{{ form.value.email }}']
                bcc: ['{{ config.email.bcc }}']
                subject: '[Feedback] {{ form.value.name }}'
                body: '{% include "forms/data.html.twig" %}'
        -
            display: thankyou
---