---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: '"Our Mission Is To Disrupt And Innovate". As Featured In iGaming Business Magazine'
   subheading: Related posts
   interview_link: http://files.igamingbusiness.co.uk/emails/iGaming_Business/iGB_97_MarApr.pdf
   next:
        related_link: Gambling Insider Magazine For ArenaCube: Sparking A Transformation
        link: news-gambling-insider
   prev:
        related_link: Fortune Greece Features ArenaCube
        link: news-fortune-greece-arenacube

news_paragraph:
    -
        text: ArenaCube's co-founder Mr. Nikkos J. Frangos explains in the latest iGaming Business issue how ArenaCube's approach to innovation has resulted in Arena Challenge, a truly disruptive game for the betting industry:
    -
        text: '"ArenaCube innovates in the realm of ideas, bringing new products to the industry, mainly in the form of on-line social games, so in a manner of speaking we have sidestepped the competition.'
    -
        text: On the one hand our games give players an alternative to the existing traditional methods of betting, thereby transferring part or perhaps on occasion all of a player’s existing betting activity. However, of greater interest is the opening of new frontiers, which expands the footprint of the industry, bringing new participants into the world of betting.
    -
        text: 'Crucially as well, our game explicitly involves our competitors, bringing them together in a spirit of true collaboration."'
    -
        text: 'You can read the full interview <a href="http://files.igamingbusiness.co.uk/emails/iGaming_Business/iGB_97_MarApr.pdf" target= "_blank">here.</a>(page 56)'        
      
related_posts:
    -
        image: arenachallenge-news.jpg
        title: ArenaCube Announces Partnership with JVsolutions
        text: ArenaCube, an international iGaming software house, announces that a new partnership has been granted with iGaming solutions supplier JVSolutions, a prominent player in the robust Italian and international market ...
        link: news-arenacube-partnership
    -
        image: fortune-greece-news.png
        title: Fortune Greece Features Arenacube
        text: The Greek edition of the international magazine Fortune hosted our co-founder for an in-depth interview regarding ArenaCube and the next steps of our innovative game Arena Challenge ...
        link: news-fortune-greece-arenacube
---

# About Us

