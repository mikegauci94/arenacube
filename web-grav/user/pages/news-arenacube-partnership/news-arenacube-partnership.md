---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: 'Arenacube Announces Partnership With JVsolutions'
   subheading: Related posts
   related_link: Fortune Greece Features ArenaCube
   link: news-fortune-greece-arenacube

news_paragraph:
    -
        text: ArenaCube, an international iGaming software house, announces that a new partnership has been granted with iGaming solutions supplier JVSolutions, a prominent player in the robust Italian and international market. Through this partnership, ArenaCube’s innovative multiplayer betting game Arena Challenge will be offered to players around the world through JVSolutions’ portfolio of betting sites.
    -
        text: Arena Challenge has been already integrated to BetOne365.com and in the following days the game will be fully integrated in JVSolutions’ portfolio which include, among others, the betting sites Potterbet.biz, Winkabet.net, Queenbetline.com and Overino.net, reaching the 15 sites in total. This partnership has enormous potential in terms of customer reach and revenue generation as the reach to the markets through these sites is vast.
    -
        text: '“We are so excited to announce today, that Arena Challenge is now part of BetOne365.com. Through our innovative game, its players can now compete, challenge others and participate in tournaments. Through our platform we have introduce the competitive element into betting and with this partnership we feel that we are one step closer to change the world of betting.” said Mario Fiorini, CEO of ArenaCube.'
    -
        text: Arena Challenge is the first ever multiplayer betting game that gives the opportunity to players to compete with each other in “tables” and multiply their profits even if some of their bet choices are not successful. It transforms sports betting into a game of skill where the top players will be rewarded through a new way of betting and competing.
    -
        text: At the same time, for the operators it is a safe and efficient tool as it eliminates risk and collateral because the winners take their winnings from the pot that is created during the progress of the game.                
      
related_posts:
    -
        image: fortune-greece-news.png
        title: Fortune Greece Features Arenacube
        text: The Greek edition of the international magazine Fortune hosted our co-founder for an in-depth interview regarding ArenaCube and the next steps of our innovative game Arena Challenge ...
        link: news-fortune-greece-arenacube
    -
        image: igaming-business-news.jpg
        title: '"Our Mission Is To Disrupt And Innovate". As Featured In iGaming Business Magazine'
        text: ArenaCube’s co-founder Mr. Nikkos J. Frangos explains in the latest iGaming Business issue how ArenaCube’s approach to innovation has resulted in Arena Challenge, a truly disruptive game for the betting industry: ...
        link: news-featured-igaming
---

# About Us

