---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: 'Fortune Grecce Features Arenacube'
   subheading: Related posts
   interview_link: http://www.fortunegreece.com/article/i-arena-pou-machonte-i-latris-tou-athlitikou-stichimatos
   next:
        related_link: “Our Mission Is To Disrupt And Innovate". As Featured In iGaming Business Magazine
        link: news-featured-igaming
   prev:
        related_link: ArenaCube Announces Partnership With JVSolutions
        link: news-arenacube-partnership

news_paragraph:
    -
        text: The Greek edition of the international magazine Fortune hosted our co-founder for an in-depth interview regarding ArenaCube and the next steps of our innovative game Arena Challenge.
    -
        text: Read the full interview <a href="http://www.fortunegreece.com/article/i-arena-pou-machonte-i-latris-tou-athlitikou-stichimatos" target= _blank>here</a> (in Greek).                  
      
related_posts:
    -
        image: arenachallenge-news.jpg
        title: Arenacube Announces Partnership with JVsolutions
        text: ArenaCube, an international iGaming software house, announces that a new partnership has been granted with iGaming solutions supplier JVSolutions, a prominent player in the robust Italian and international market ...
        link: news-arenacube-partnership
    -
        image: igaming-business-news.jpg
        title: '"Our Mission Is To Disrupt And Innovate". As Featured In iGaming Business Magazine'
        text: ArenaCube’s co-founder Mr. Nikkos J. Frangos explains in the latest iGaming Business issue how ArenaCube’s approach to innovation has resulted in Arena Challenge, a truly disruptive game for the betting industry: ...     
        link: news-featured-igaming
---


