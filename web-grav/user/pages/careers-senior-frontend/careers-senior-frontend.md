---
menu: Careers
title: 'ArenaCube - News ArenaCube Partnership'
description: 'ArenaCube offers an integrated portfolio of gaming systems and product solutions & services.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
careers:
   heading: 'Senior Front-End Developer'
   subheading: Related posts
   list_title:
        first: Description:
        second: Qualifications:
        third: Additional skill set is considered a plus:
        fourth: Location
   related_link: System Administrator
   link: careers-system-administrator

careers_content:
    -
        text: Experience in advanced web technologies and mobile UX/UI. Develop and test across multiple browser, platforms and devices such as tablets and smart-phones for responsive designs.
    -
        text: Experience with development version control systems such as GIT.
    -
        text: Intermediate or better PHP skills with solid understanding of object-oriented programming (OOP).
    -
        text: Knowledge of MVC frameworks.
    -
        text: Proficient in HTML5, CSS3, LESS/SASS and XHTML.
    -
        text: Creating enriched user experiences with Javascript frameworks such as JQuery.
    -
        text: Develop new user facing features based on PSD mock-ups and wireframes.
    -
        text: The ability to multitask and adhere to a fast-paced, deadline-driven environment.

careers_content_second:
    -
        text: 4 years of experience or more with JQuery, JSON, AJAX, HTML5, CSS3, PHP and Javascript.
    -
        text: Good team player whilst being able to work on his/her owns initiative, also flexible and a keen eye for detail.
        
careers_content_third:
    -
        text: Experience building mobile web applications.
    -
        text: AngularJS hands-on experience would be considered an asset.
careers_content_fourth:
    -
        text: Malta.
      
related_posts:
    -
        image: careers-sysadmin.jpg
        title: System Administrator
        text: The Greek edition of the international magazine Fortune hosted our co-founder for an in-depth interview regarding ArenaCube and the next steps of our innovative game Arena Challenge ...
        link: careers-system-administrator

---

# About Us

