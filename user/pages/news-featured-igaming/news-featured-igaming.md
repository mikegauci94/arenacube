---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: '"Our mission is to disrupt and innovate". As featured in iGaming business magazine'
   subheading: Related posts
   interview_link: http://files.igamingbusiness.co.uk/emails/iGaming_Business/iGB_97_MarApr.pdf
   next:
        related_link: Gambling insider magazine for ArenaCube: Sparking a transformation
        link: news-gambling-insider
   prev:
        related_link: Fortune Greece features ArenaCube
        link: news-fortune-greece-arenacube

news_paragraph:
    -
        text: ArenaCube's co-founder Mr. Nikkos J. Frangos explains in the latest iGaming Business issue how ArenaCube's approach to innovation has resulted in Arena Challenge, a truly disruptive game for the betting industry:
    -
        text: '"ArenaCube innovates in the realm of ideas, bringing new products to the industry, mainly in the form of on-line social games, so in a manner of speaking we have sidestepped the competition.'
    -
        text: On the one hand our games give players an alternative to the existing traditional methods of betting, thereby transferring part or perhaps on occasion all of a player’s existing betting activity. However, of greater interest is the opening of new frontiers, which expands the footprint of the industry, bringing new participants into the world of betting.
    -
        text: 'Crucially as well, our game explicitly involves our competitors, bringing them together in a spirit of true collaboration."'
    -
        text: 'You can read the full interview <a href="http://files.igamingbusiness.co.uk/emails/iGaming_Business/iGB_97_MarApr.pdf" target= "_blank">here.</a>(page 56)'        
      
---
