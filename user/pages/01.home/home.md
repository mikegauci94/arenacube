---
menu: Home
title: 'Online Gaming and Sports Betting Software Company - ArenaCube'
description: 'Online iGaming Software Company offering products such as the multiplayer betting platform Arena Challenge, a fully managed sportsbook and a high performance Gaming Platform.'
keywords: 'Sports Betting, Online Gaming Software, Gambling business, Online iGaming Software, Sportsbook, Gaming platform, Multiplayer betting platform, Betting products, Gaming products, Gaming management system, Online Gaming, iGaming, Gaming market, Regulated Market'
home_banner:
    header_top: Leading for a new era
    header_bottom: Arena Challenge shortlisted for Digital Gaming Innovation in the<br>Global Gaming Awards 2016.
    image: shortlisted.png
    button: Read more
    link: news-digital-gaming
    
home_arena_challenge_caption:
    title: Arena<br>Challenge
    sub_title: The game that's got everybody talking
    description: Arena Challenge is the first in a series of innovative live betting games designed to re-write the future of the industry.
    button: Read more
    link: arena-challenge
    
home_sportsbook_caption:
    title: Arena<br>Sportsbook
    description: ArenaCube offers a top-notch sports betting solution that combines classic betting features with an enhances uder interface to deliver one of the most user-friendly sportsbook on the market.
    button: Read more
    link: sportsbook
    
partners:
    title: Partners
featured:
    title: As Featured On
    link_one: https://www.gamblinginsider.com/Magazine/SportsBettingFocus3/?Mid=55
    link_two: http://files.igamingbusiness.co.uk/emails/iGaming_Business/iGB_97_MarApr.pdf
    link_three: http://www.fortunegreece.com/article/i-arena-pou-machonte-i-latris-tou-athlitikou-stichimatos/
arena_challenge_features:
    -
        title: '110k'
        description: 'Challengers'
        image: stats-players.png
        link: services
    -
        title: '240k'
        description: 'Real Buy-ins'
        image: stats-buyins.png
        link: services
    -
        title: '400k'
        description: 'Fun Buy-ins'
        image: stats-fun.png
        link: services
        arena_challenge_features:
---



