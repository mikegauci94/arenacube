---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: Arena Challenge shortlisted for Digital Gaming Innovation in the Global Gaming Awards 2016
   subheading: Related posts
   link: http://www.icetotallygaming.com/exhibitor/arenacube
  
   prev:
        related_link: ArenaCube at ICE Totally Gaming 2016
        link: news-arenacube-ice

news_paragraph:
    -
        text: ArenaCube has been shortlisted for the Global Gaming Awards 2016 in the Digital Gaming Innovation category for its revolutionary multiplayer betting game Arena Challenge.
    -
        text: The Global Gaming Awards were created to recognise and reward innovation and achievement across both the digital and land-based gaming sectors, with awards presented across 12 industry-wide categories. Now entering their third year, the Awards will be voted on by a truly independent panel of 60 judges, taken from a range of industry sectors and geographies.
    -
        text: The Digital Gaming Innovation category recognizes and champions companies which are striving to meet the ever-growing demands and expectations of discerning digital players and ArenaCube’s innovative multiplayer sports betting product offer truly disruptive advantages to small, medium or large sports betting operators that increase their profits, eliminate their risk exposure and boost the prospects of winnings for players.
    -
        text: '“To be one of the 10 companies shortlisted in this category is a great honour. For us this achievement is a recognition for the passion, the creativity and the expertise that our company has to offer to the industry.” said Nikkos Frangos, Co-Founder and President of ArenaCube.'
    -
        text: The award ceremony will be held on the 28th September at the Titian Ballroom in the Venetian Palazzo Congress Center, Las Vegas.        

---


