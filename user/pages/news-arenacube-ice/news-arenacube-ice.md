---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: ArenaCube at ICE Totally Gaming 2016
   bold: ArenaCube will showcase Arena Challenge at ICE Gaming 2016
   subheading: Related posts
   link: http://www.icetotallygaming.com/exhibitor/arenacube
   next:
        related_link: Arena Challenge shortlisted for Digital Gaming Innovation in the Global Gaming Awards 2016
        link: news-digital-gaming
   prev:
        related_link: “Our mission is to disrupt and innovate". As featured in iGaming business magazine
        link: news-featured-igaming

news_paragraph:
    -
        text: ArenaCube will have a dynamic presence with its own stand (S1-110), for the 3rd consecutive year, in the upcoming ICE Totally Gaming Exhibition that will be held February 2-4 2016 in London.
    -
        text: Following a year of continuous and robust progress and development, the innovative game Arena Challenge will be the flagship of ArenaCube this year. It is the first ever multiplayer betting game that gives the opportunity to players to compete with each other and multiply their profits even if some of their bet choices are not successful. Arena Challenge transforms sports betting into a game of skill where the top players will be rewarded.
    -
        text: At the same time, for the operators it is a safe and efficient tool as it eliminates risk and collateral, and therefore drastically reduces set-up and operating costs. Additionally, it offers their customers a new and innovative way of betting and competing. Currently, nine betting operators have included the game in their portfolio, and the users playing the game are increasing emphatically on a monthly basis. During the exhibition a live tournament will take place so that the visitors can experience the game first hand.
    -
        text: Additionally, industry professionals visiting ArenaCube’s stand (S1-110) will have the opportunity to meet with company representatives and be informed about its innovative products and services, such as the fully customizable and state of the art Sportsbook or the AvacsPro, an integrated back-end management system that combines cutting-edge technology with a high-quality UI for the user.
    -
        text: ArenaCube strategically aims to redefine the sports-betting industry and become a world-leading innovator, with a stream of innovations both online and land-based.        

---


