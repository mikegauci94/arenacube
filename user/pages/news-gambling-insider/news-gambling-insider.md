---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: Gambling insider magazine for ArenaCube: Sparking a transformation
   subheading: Related posts
   interview_link: https://www.gamblinginsider.com/Magazine/SportsBettingFocus3/?Mid=55
   next:
        related_link: ArenaCube at ICE Totally Gaming 2016
        link: news-arenacube-ice
   prev:
        related_link: “Our mission is to disrupt and innovate". As featured in iGaming business magazine
        link: news-featured-igaming

news_paragraph:
    -
        text: Our co-founder Mr. Nikkos J. Frangos discusses our attempts to innovate betting without corrupting its roots: "..It was vital for us to keep the roots of betting uncorrupted, and innovate around these roots, whilst introducing competition in a way never done before. But even more than any of these was the yearning of novelty, and the passion to spark a transformation that the betting world needs…"
    -
        text: 'Read the full interview <a href="https://www.gamblinginsider.com/Magazine/SportsBettingFocus3/?Mid=55" target= "_blank"> here</a>'      

---

