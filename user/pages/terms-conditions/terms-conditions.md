---
menu: Careers
title: 'ArenaCube - About us'
description: 'ArenaCube offers an integrated portfolio of gaming systems and product solutions & services.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
terms:
   heading: 'Terms & Conditions'

terms_contents:
    -
        text: The following conditions apply to anyone accessing and using ARENA CUBE website (Website), or any part of it. ARENA CUBE may modify these conditions from time to time and accordingly you are advised to keep up to date with the changes by regularly reviewing the conditions. By using the Website you agree to be bound by these conditions.
    -
        text: The content available on the Website is for informational interest only and ARENA CUBE makes every effort to ensure the accuracy and completeness of the information given, but can offer no guarantees.
    -
        text: It is your responsibility to judge the accuracy or completeness of the content before relying on it in any way. Your use of the content (for whatever purpose) is at your sole risk. The e-mail address for sending complaints about any content on the Website is here. Thank you for your understanding.
    -
        text: The content is protected by copyright, trademark and other intellectual property rights, as applicable and is provided solely for your own use. Republication or redistribution of any of the content available on the Website including by framing or similar means is prohibited.
    -
        text: ARENA CUBE excludes all liability of any kind (including negligence) in respect of any third party information or other material made available on, or which can be accessed using, the Website.
    -
        text: You are solely responsible for evaluating any goods (including software) or services offered by third parties via the Website or on the Internet. ARENA CUBE will not be a party to or in any way responsible for any transactions between you and third parties. Any personal information you may wish to disclose to third parties is at your own risk.                
     
---