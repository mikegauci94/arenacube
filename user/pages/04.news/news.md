---
menu: News
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
    heading: 'News'
    
news_feed:
    -
        title: Arena Challenge shortlisted for Digital Gaming Innovation in the Global Gaming Awards 2016
        date: 06.07.2016
        text: ArenaCube has been shortlisted for the Global Gaming Awards 2016 in the Digital Gaming Innovation category for its revolutionary multiplayer betting game Arena Challenge ...
        button: More
        link: news-digital-gaming
    -
        title: ArenaCube announces partnership with JVsolutions
        date: 19.05.2016
        text: ArenaCube, an international iGaming software house, announces that a new partnership has been granted with iGaming solutions supplier JVSolutions, a prominent player in the robust Italian and international market ...
        button: More
        link: news-arenacube-partnership
    -
        title: Fortune Greece features ArenaCube
        date: 24.03.2016
        text: The Greek edition of the international magazine Fortune hosted our co-founder for an in-depth interview regarding ArenaCube and the next steps of our innovative game Arena Challenge ...
        button: More
        link: news-fortune-greece-arenacube
    -
        title: '"Our mission is to disrupt and innovate". As featured in iGaming business magazine'
        date: 21.03.2016
        text: ArenaCube’s co-founder Mr. Nikkos J. Frangos explains in the latest iGaming Business issue how ArenaCube’s approach to innovation has resulted in Arena Challenge, a truly disruptive game for the betting industry: ...
        button: More
        link: news-featured-igaming
    -
        title: Gamling insider magazine for ArenaCube; Speaking a transformation
        date: 24.02.2016
        text: Our co-founder Mr. Nikkos J. Frangos discusses our attempts to innovate betting without corrupting its roots: “..It was vital for us to keep the roots of betting uncorrupted, and innovate around these roots, whilst introducing competition in a way never done before ...
        button: More
        link: news-gambling-insider
    -
        title: ArenaCube at ICE Totally Gaming 2016
        date: 01.12.2015
        text: ArenaCube will showcase Arena Challenge at ICE Totally Gaming 2016 ArenaCube will have a dynamic presence with its own stand (S1-110), for the 3rd consecutive year, in the upcoming ICE Totally Gaming Exhibition that will be held February 2-4 2016 in London ...
        button: More
        link: news-arenacube-ice      

main_news_feed_first:
    -
        image: digital-gaming-news.jpg
        title: Arena Challenge shortlisted for Digital Gaming Innovation in the Global Gaming Awards 2016
        text: ArenaCube has been shortlisted for the Global Gaming Awards 2016 in the Digital Gaming Innovation category for its revolutionary multiplayer betting game Arena Challenge ...
        date: 06.07.2016
        button: More
        link: news-digital-gaming
    -
        image: arenachallenge-news.jpg
        title: ArenaCube announces partnership with JVsolutions
        text: ArenaCube, an international iGaming software house, announces that a new partnership has been granted with iGaming solutions supplier JVSolutions, a prominent player in the robust Italian and international market ...
        date: 19.05.2016
        button: More
        link: news-arenacube-partnership    
        
main_news_feed_second:
    -
        image: fortune-greece-news.png
        title: Fortune Greece features ArenaCube
        text: The Greek edition of the international magazine Fortune hosted our co-founder for an in-depth interview regarding ArenaCube and the next steps of our innovative game Arena Challenge ...
        date: 24.03.2016
        button: More
        link: news-fortune-greece-arenacube
    -
        image: igaming-business-news.jpg
        title: '"Our mission is to disrupt and innovate". As featured in iGaming business magazine'
        text: ArenaCube’s co-founder Mr. Nikkos J. Frangos explains in the latest iGaming Business issue how ArenaCube’s approach to innovation has resulted in Arena Challenge, a truly disruptive game for the betting industry: ...
        date: 21.03.2016
        button: More
        link: news-featured-igaming

main_news_feed_third:
    -
        image: gambling-insider-news.jpg
        title: Gamling insider magazine for ArenaCube; Speaking a transformation
        text: Our co-founder Mr. Nikkos J. Frangos discusses our attempts to innovate betting without corrupting its roots: “..It was vital for us to keep the roots of betting uncorrupted, and innovate around these roots, whilst introducing competition in a way never done before ...
        date: 24.02.2016
        button: More
        link: news-gambling-insider
    -
        image: arenacube-ice-news.png
        title: ArenaCube at ICE Totally Gaming 2016
        text: ArenaCube will showcase Arena Challenge at ICE Totally Gaming 2016 ArenaCube will have a dynamic presence with its own stand (S1-110), for the 3rd consecutive year, in the upcoming ICE Totally Gaming Exhibition that will be held February 2-4 2016 in London ...
        date: 01.12.2015
        button: More
        link: news-arenacube-ice
    

---


