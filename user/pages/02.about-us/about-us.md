---
menu: About us
title: 'About us - ArenaCube'
description: 'Online iGaming Software Company offering products such as the multiplayer betting platform Arena Challenge, a fully managed sportsbook and a high performance Gaming Platform.'
about:
   first_heading: 'The next milestone in the betting world'
   para_1: 'ArenaCube is a software development company with global outlook and reach offering software services to the iGaming industry.'
   para_2: 'The yearning for novelty and the passion to spark a transformation that the betting world needs, led a team of visionary professionals to found the company in 2013. Our vision is, and will be, to usher in an entirely new betting philosophy and re-shape the future of the global betting and gaming industry.'
   para_3: 'With three offices across Europe, we are focused in developing and delivering cutting edge solutions for gaming operators at any stage of their development, including the multiplayer betting platform, Arena Challenge, a real game changer for the market; WiderPoker, the first in a series of multiplayer video poker games; as well as the highly modular and scalable Gaming Platform.'
   para_4: 'With our innovative products we offer a truly disruptive advantage to small, medium or large operators by increasing the players’ engagement levels and experience, significantly reducing operational costs and eliminating the need for risk management.'
   para_5: 'Our services and software solutions present a differentiating factor; as each customer is unique, in this concept each project has its own special needs.'
   para_6: 'Thus, our business is customer-centric, our services are continuously tuned, and our software packages are totally customizable.'
   
   second_heading: 'Re-live the excitment'
   text: 'ArenaCube’s vision it’s to re-shape the future of the global betting industry. Since the advent of history, the human need to compete against each other – and to outrank each other – has been etched into our DNA. ArenaCube wanted to harness this powerful universal spirit of betting, along with its main motivating force, and to use it as the bedrock of a rewards-based social game.'
   
   third_heading: Watch the fusion of betting & gaming unfold before your eyes
   para_7: The ArenaCube philosophy travels far beyond the constraints of classic-mode betting. It sought to make betting and gaming more prestigious and to introduce the extreme innovation that the founders saw has been lacking in the industry since its inception. In doing so, they believed they would create a multi-faceted package of superior advantages for both players and operators.
   para_8: For that reason, it was decided to introduce a revolutionary new era of social-wagering via an invigorating first game, built exclusively by the ArenaCube team. A live version of betting that perfectly demonstrates the break-through ArenaCube philosophy.
   para_9: ArenaCube registered the required patents, and secured all necessary licenses, enabling the platform to be both legal and in accordance with the regulations and legislation, while delivering the highest standards available in the market.
   
   fourth_heading: With Arenacube everything is possible
   
   fifth_heading: Certifications
   para_10: Gaming licenses in regulated markets are unquestionably a crucial requirement. ArenaCube has selected Malta and Curacao as its preferred license jurisdictions. Additionally, Arena Cube constantly monitors other jurisdictions to apply for licenses wherever it deems it relevant for operational purposes.
   
about_arenacube_possibilities:
    -
        title: Company's Advantages
        first_subheading: Re-defined risk management
        first_text: Running a betting business demands employing a costly team of risk professionals to assess the potential bets one can accept. ArenaCube gives you the possibility of eliminating this expense since from now on, players will be battling it out between themselves while you earn your commissions.
        second_subheading: No longer required to stock huge liquid reserves
        second_text: With ArenaCube’s ground-breaking games, players win their opponents’ money so the operator is no longer obligated to cover possible winnings.
    -
        title: Player's Advantages
        first_subheading: Miss out on 1, 2 or more bets - and still win with unrivalled odds
        first_text: In order to win – regardless of the number of failed selections – all you need is to have a greater combination of winning odds than your opponents. And, the amount of money you win no longer depends on the winning odds. Instead, it is determined by the size of the pot created by your opponents’ bets.
        second_subheading: Dramatically increase the potential multiplier for winnings
        second_text: Arena Challenge is open to every sports betting enthusiast in the world, and thus players have the potential to challenge as many opponents as they like. By selecting higher winning odds than their opponents, players can dramatically multiply their winnings with respect to traditional betting… But in a familiar setting that utilises all your usual betting skills and strategies.
        
financial:
        title: Financial Data
        text: ArenaCube Financial Data 2015
        link: ArenacubeBalanceSheet.pdf
     
---

