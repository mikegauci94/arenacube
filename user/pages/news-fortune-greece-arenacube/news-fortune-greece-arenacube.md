---
title: 'News - ArenaCube'
description: 'Learn about the latest news and company developments from ArenaCube.'
keywords: 'igaming, intelligence, gaming, online gaming, Information Technology, Technology, Arena Challenge, Communication Technologies, Information technologies, Business Software, content management Business Intelligence'
news:
   heading: 'Fortune Grecce features Arenacube'
   subheading: Related posts
   interview_link: http://www.fortunegreece.com/article/i-arena-pou-machonte-i-latris-tou-athlitikou-stichimatos
   next:
        related_link: “Our mission is to disrupt and innovate". As featured in iGaming business magazine
        link: news-featured-igaming
   prev:
        related_link: ArenaCube announces partnership with JVSolutions
        link: news-arenacube-partnership

news_paragraph:
    -
        text: The Greek edition of the international magazine Fortune hosted our co-founder for an in-depth interview regarding ArenaCube and the next steps of our innovative game Arena Challenge.
    -
        text: Read the full interview <a href="http://www.fortunegreece.com/article/i-arena-pou-machonte-i-latris-tou-athlitikou-stichimatos" target= _blank>here</a> (in Greek).                  

---


